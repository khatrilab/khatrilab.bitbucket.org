### What is this repository for? ###

This repository contains the Khatrilab's package documentations making it accessible as website.

MetaIntegrator documentation: http://khatrilab.bitbucket.org/MetaIntegratorVignette/

### How to add further pages: ###

To add further HTML docs see: https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket